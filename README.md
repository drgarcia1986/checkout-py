checkout-py
========

## Install pypy
Download from [oficial site](http://pypy.org/download.html) and unpack/test with this command:

```bash
$ tar xf pypy-2.5.1.tar.bz2
$ pypy --version
Python 2.7.9 (9c4588d731b7, Mar 23 2015, 16:30:30)
[PyPy 2.5.1 with GCC 4.6.3]
```

## Virtualenv with pypy
Using virtualenv with `python` option:

```bash
$ virtualenv -p pypy-2.5.1/bin/pypy venv-pypy
```

## Requirements
Just run this command:

```bash
$ make requirements-dev
```

## Runserver
Another simple command :smile: :
```bash
$ make runserver-pypy
```
